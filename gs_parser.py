#!/usr/bin/python
from struct import unpack
import sys
import math
from datetime import datetime

# parameters
sound_speed = 1500.0
port_offset = 60.0
stbd_offset = -60.0

filename = sys.argv[1]
chan = 'none'
if len(sys.argv) == 3:
  chan = sys.argv[2]

fin = open(filename, 'rb')
if chan == 'none':
  fhsx = open(filename + '.hsx', 'w')
else:
  fhsx = open(filename + '.' + chan + '.hsx', 'w')

# output header info
fhsx.write('FTP NEW 2\n')
fhsx.write('HSX 7\n')
fhsx.write('DEV 0 4 "GPS"\n')
fhsx.write('DV2 0 0004 0 1\n')
fhsx.write('DEV 1 512 "IMU"\n')
fhsx.write('DV2 1 0200 0 1\n')
fhsx.write('DEV 2 32 "GYRO"\n')
fhsx.write('DV2 2 0020 0 1\n')
fhsx.write('DEV 3 16 "DEPTH"\n')
fhsx.write('DV2 3 0200 0 1\n')
fhsx.write('DEV 4 32784 "GeoSwath"\n')
fhsx.write('DV2 4 0001 0 1\n')
fhsx.write('MBI 4 2 0000 0881 1379 0 0.000 0.000\n')
fin.seek(0,2)
eof = fin.tell()
fin.seek(0,0)

# File Header
creation = unpack('I', fin.read(4))[0]
date_str = datetime.fromtimestamp(creation).strftime('%H:%M:%S %m/%d/%Y')
fhsx.write("TND {}\n".format(date_str))
fhsx.write("EOH\n")
raw_header_size = unpack('h', fin.read(2))[0]
ping_header_size = unpack('h', fin.read(2))[0]
fin.read(512)
freq = unpack('i', fin.read(4))[0]
echo_type = unpack('h', fin.read(2))[0]
fin.read(18)

wl = 1.0 / freq
while fin.tell() < eof:
  # ping header
  ping_num = unpack('i', fin.read(4))[0]
  ping_time = unpack('d', fin.read(8))[0]
  prev_ping_pos = unpack('i', fin.read(4))[0]
  ping_size = unpack('i', fin.read(4))[0]
  nav_num = unpack('b', fin.read(1))[0]
  att_num = unpack('b', fin.read(1))[0]
  hdg_num = unpack('b', fin.read(1))[0]
  echo_num = unpack('b', fin.read(1))[0]
  svs_num = unpack('b', fin.read(1))[0]
  aux1_num = unpack('b', fin.read(1))[0]
  aux2_num = unpack('b', fin.read(1))[0]
  ping_len = unpack('h', fin.read(2))[0]
  pulse_len = unpack('b', fin.read(1))[0]
  power = unpack('b', fin.read(1))[0]
  ss_gain = unpack('b', fin.read(1))[0]
  sample_num = unpack('i', fin.read(4))[0]
  side = unpack('b', fin.read(1))[0]
  fin.read(27)

  write = False
  if chan == 'none':
    write = True
  elif chan == 'port' and side == 0:
    write = True
  elif chan == 'stbd' and side == 1:
    write = True

  # RAA records
  angles = []
  ranges = []
  amplitudes = []
  raa = sample_num * 3 * 2
  for n in range(0,sample_num):
    raa_time = unpack('h', fin.read(2))[0]
    raa_sine = unpack('h', fin.read(2))[0]
    raa_amp = unpack('h', fin.read(2))[0]
    r = raa_sine/32000.0
    r = min(1.0, r)
    r = max(-1.0, r)
    #print raa_time,
    if side == 0:
      angles.append(port_offset + -1.0*math.degrees(math.asin(r)))
    else:
      angles.append(stbd_offset + math.degrees(math.asin(r)))
    ranges.append(raa_time * wl * sound_speed)
    amplitudes.append(raa_amp)
  
  dt = datetime.fromtimestamp(ping_time)

  if write:
    fhsx.write('RMB 4 {} 2 0000 0881 {} {} {}\n'.format((dt - dt.replace(hour=0, minute=0, second=0, microsecond=0)).total_seconds(),sample_num, sound_speed, ping_num)) 
    for r in ranges:
      fhsx.write('{} '.format(r))
    fhsx.write('\n')
    for a in angles:
      fhsx.write('{} '.format(a))
    fhsx.write('\n')
    for i in amplitudes:
      fhsx.write('{} '.format(i))
    fhsx.write('\n')

  # NAV records
  nav = nav_num * (8 + 8 + 4 + 8 + 8 + 1)
  for n in range(0,nav_num):
    nav_east = unpack('d', fin.read(8))[0]
    nav_north = unpack('d', fin.read(8))[0]
    nav_height = unpack('f', fin.read(4))[0]
    nav_gps_time = unpack('d', fin.read(8))[0]
    nav_time = unpack('d', fin.read(8))[0]
    nav_qual = unpack('b', fin.read(1))[0]
    dt = datetime.fromtimestamp(nav_time)
    if write:
      fhsx.write('POS 0 {} {} {}\n'.format((dt - dt.replace(hour=0, minute=0, second=0, microsecond=0)).total_seconds(), nav_east, nav_north))


  # MRU records
  mru = att_num * (4 + 4 + 4 + 8)
  for n in range(0,att_num):
    mru_roll = unpack('f', fin.read(4))[0]
    mru_pitch = unpack('f', fin.read(4))[0]
    mru_heave = unpack('f', fin.read(4))[0]
    mru_time = unpack('d', fin.read(8))[0]
    dt = datetime.fromtimestamp(mru_time)
    if write:
      fhsx.write('HCP 1 {} {} {} {}\n'.format((dt - dt.replace(hour=0, minute=0, second=0, microsecond=0)).total_seconds(), mru_heave, mru_roll, mru_pitch))

  # HDG records
  hdg = hdg_num * (4 + 8)
  for n in range(0,hdg_num):
    hdg_gyr = unpack('f', fin.read(4))[0]
    hdg_time = unpack('d', fin.read(8))[0]
    dt = datetime.fromtimestamp(hdg_time)
    if write:
      fhsx.write('GYR 2 {} {}\n'.format((dt - dt.replace(hour=0, minute=0, second=0, microsecond=0)).total_seconds(), hdg_gyr))

  # skip rest of record
  fin.read(ping_size - raa - nav - mru - hdg)
